#!/usr/bin/env python3

from datetime import datetime, timedelta
import sys
import csv
import os

station_number = sys.argv[1]
station_number_file = csv.reader(open('{}/data/data.csv'.format(sys.path[0]), 'r'), delimiter=',')

for row in station_number_file:
    if station_number == row[1]:
        station_id = row[1]
        station_name = row[2]
        all_lines = row[0].split(',')
        print(row)
        unique_lines = list(set(all_lines))

working_lines = []

for line in unique_lines:
    try:
        if os.path.isfile('{}/data/st/{}.csv'.format(sys.path[0], line)):
            working_lines.append(line)
    except:
        continue

for line in working_lines:

#    print('line: {}'.format(line))
#    print('station_id: {}'.format(station_id))
#    print('name: {}\n'.format(station_name))

    start_time_workday = []
    start_time_saturday = []
    start_time_sunday = []

    
    try:
        with open('{}/data/st/{}.csv'.format(sys.path[0], line), 'r') as f:     
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader:
                hour_num = int(row[0])
                hour = row[0].split(' ')
                minutes_workday = row[1].split(' ')
                minutes_saturday = row[2].split(' ')
                minutes_sunday = row[3].split(' ')

                for element in minutes_workday:
                    if hour_num < 10:
                        clockw = '0{}:{}'.format(str(hour[0]), element)
                    else:
                        clockw = '{}:{}'.format(str(hour[0]), element)
                    start_time_workday.append(clockw.strip('*'))
                
                for element in minutes_saturday:
                    if hour_num < 10:
                        clockt = '0{}:{}'.format(str(hour[0]), element)
                    else:
                        clockt = '{}:{}'.format(str(hour[0]), element)
                    start_time_saturday.append(clockt.strip('*'))

                for element in minutes_sunday:
                    if hour_num < 10:
                        clockn = '0{}:{}'.format(str(hour[0]), element)
                    else:
                        clockn = '{}:{}'.format(str(hour[0]), element)
                    start_time_sunday.append(clockn.strip('*'))

#        print(start_time_workday)
        
        with open('{}/data/tta/{}.csv'.format(sys.path[0], line), 'r') as f:
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader:
                if station_number == row[1]:
                    delay = row[0]

        current_time = datetime.now().strftime('%H:%M')

#        print('current_time: {}'.format(current_time))

        best_guess = []

        for item in start_time_workday: # check if saturday, sunday
            start_time_hour = int(str(item[:2:]))
            start_time_minute = int(str(item[-2:]).strip("*"))
            time_to_test = timedelta(hours=start_time_hour, minutes=start_time_minute) + timedelta(minutes=int(delay))
            current_hour = int(current_time[:2:])
            current_minute = int(current_time[-2:])
            current_delta = timedelta(hours=current_hour, minutes=current_minute)
            if time_to_test > current_delta:
                best_guess.append(time_to_test)

        my_choice = str(best_guess[0])[:-3]

        arrives = timedelta(hours=int(my_choice[:1]), minutes=int(my_choice[-2:])) - timedelta(hours=int(current_time[:1]), minutes=int(current_time[-2:]))

#        print('time_of_arrival: {}'.format(my_choice))
#        print('arrives in: {}min'.format(str(arrives)[2:4]))
        print('{}:{}min'.format(line.rjust(len(max(working_lines, key=len))), str(arrives)[2:4]))
    except:
#        print('{}:n/a'.format(line))
        continue
