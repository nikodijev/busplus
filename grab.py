#!/usr/bin/env python3

import sys
import csv
import re
import requests
from bs4 import BeautifulSoup
import os

links = '{}/data/links'.format(sys.path[0])

if not os.path.isdir('{}/data/st'.format(sys.path[0])):
    os.makedirs('{}/data/st'.format(sys.path[0]))
    print("created directory: st")

if not os.path.isdir('{}/data/tta'.format(sys.path[0])):
    os.makedirs('{}/data/tta'.format(sys.path[0]))
    print("created directory: tta")


with open(links, 'r') as file:
    for line in file:
        url = line.rstrip()
        f = url[41:46]
        filename = f.split('-', 1)[0]
        file_path = '{}/data/st/{}.csv'.format(sys.path[0], filename)
        
        if not os.path.isfile(file_path):
        
            page = requests.get(url).text
            soup = BeautifulSoup(page, 'lxml')
            order = soup.find_all('div', {'class': re.compile('^wpb_text_column wpb_content_element.*')})

            line = []

            for ul in order:
                for li in ul.findAll('li'):
                    line.append(li.text)
            del line[0]

            times = []
            ids = []
            names = []

            for item in line:
                times.append(item[3:5])
                try:
                    ids.append(re.search(r'[#]\w+', item).group(0)[1:])
                except:
                    pass
                names.append(' '.join(item.split()[3:]))

            full_list = zip(times, ids, names)
            
            print('{}'.format(url))

            with open('{}/data/tta/{}.csv'.format(sys.path[0], filename), 'wt') as f:
                csv_writer = csv.writer(f)
                for row in full_list:
                    csv_writer.writerow(row)
            
            try:
                tbody = soup.find_all('tbody', {'class':'row-hover'})[1]
            except:
                tbody = soup.find_all('tbody', {'class':'row-hover'})[0]
            rows = tbody.find_all('tr')
            times = []
            
            for row in rows:
                cols = row.find_all('td')
                cols = [x.text.strip() for x in cols]
                times.append(cols)

            with open('{}/data/st/{}.csv'.format(sys.path[0], filename), 'wt') as f:
                csv_writer = csv.writer(f)
                for row in times:
                    csv_writer.writerow(row)
