# busplus

don't wanna pay 2.76 RSD to find out what is your (belgrade gsp) bus ETA? there is a way.


~ what is this? ~

all information about the paid service can be found here: https://www2.busplus.rs/lt/ussd-servis


~ what have I done? ~

* reverse engineered this app: https://play.google.com/store/apps/details?id=com.busplus&hl=en_us
* extracted database with station name, station id and bus lines for all stations in belgrade
* scraped start time and arrival time for each bus line and each station from: https://www.busevi.com/
* wrote some python code

~ is this done? ~

it is not. haven't implemented a test case for saturday, sunday and state holidays (out of laziness and lack of interest).
original plan was to rewrite it in cotlin as a free adroid app, but I don't have the motivation now.

~ so? ~

so, it was fun doing all this.